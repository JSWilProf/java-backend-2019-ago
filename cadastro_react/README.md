## Cadastro React

# Projeto deacoplado e convertido de Thymeleaf com Bootstrap e CSS3 para React

. Neste projeto foram incluídos os seguintes pacotes:

  - React-Redux, Redux
  - React-Bootstrap, Bootstrap
  - React-Router, React-Router-Dom, React-Router-Bootstrap
  - React-FontAwesome, FontAwesome-Svg-Core, Free-Solid-Svg-Icons
  - Yup
  - Formik
  - Axios

. Ele utiliza como Backend uma aplicação Java criada com Spring Boot

. Para reconstruir a aplicação

    - Instale o Visual Studio Code
    - Instale o node.js
    - Abra o projeto cadastro_react
    - Abra um terminal no Visual Studio
    - No terminal executo o comando: npm install
    - Após a execução do comando anterior faça: npm run start