import React, { Component } from 'react';
import { Col, Form, Button, Alert } from 'react-bootstrap';
import { Formik } from 'formik';
import * as yup from 'yup';
import axios from '../store/axios';
import { Link } from 'react-router-dom';

class CadServico extends Component {
    state = {
        showAlert: false,
        tipoAlerta: "success",
        mensagem: 'Cadastrado com Sucesso',
        idServico: this.props.match.params.id,
        servico: {
            nome: ''
        }
    }

    handleSubmit(values, {resetForm, setErrors, setSubmitting}) {
        axios.post('/salvaServico', {
            idServico: this.state.idServico,
            nome: values.nome
        }).then(response => {
            if(this.state.idCliente) {
                this.props.history.push('/lstservico');
            } else {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "success",
                    mensagem: 'Cadastrado com Sucesso',
                });
                resetForm();
                setSubmitting(false); 
            }
        }).catch(reason => {
            if(reason.response.status === 422) {
                setErrors(reason.response.data)
            } else {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "danger",
                    mensagem: reason.response.status === 403
                        ? 'Sem autorização para efetuar o Cadastro'
                        : 'Falha ao efetuar o Cadastro'
                });
            }
            setSubmitting(false); 
        });
    }

    handleDismiss = () => this.setState({ showAlert: false });

    render() {
        const schema = yup.object().shape({
            nome: yup.string()
                .min(5, 'O nome deve ter mais que 5 caracteres')
                .max(150, 'Não entre com mais de 150 caracteres')
                .required('O nome deve ser informado')
        });

        return (
            <Formik
                enableReinitialize
                validationSchema={schema}
                onSubmit={this.handleSubmit.bind(this)}
                initialValues={this.state.servico}>
                {({handleSubmit, handleChange, values, touched, errors, isSubmitting}) => (
                    <div>
                        <Alert variant={this.state.tipoAlerta} onClose={this.handleDismiss} dismissible 
                            show={this.state.showAlert}>
                            <strong>{this.state.mensagem}</strong>
                        </Alert>
                        <Col className="mb-4 shadow p-4 quadro">
                            <Form noValidate onSubmit={handleSubmit}>
                                <div className="d-flex justify-content-center">
                                    <fieldset style={{flex: 1}}>
                                        <div>
                                            <h2>Cadastra Serviço</h2>
                                        </div>

                                        <Form.Group controlId="v01">
                                            <Form.Label>Nome</Form.Label>
                                            <Form.Control type="text" placeholder="Informe o Nome do Serviço"
                                                name="nome" value={values.nome} onChange={handleChange}
                                                isInvalid={touched.nome && errors.nome} />
                                            <Form.Control.Feedback type="invalid">{errors.nome}</Form.Control.Feedback>
                                        </Form.Group>
                                        <div>
                                            <Button type="submit" value="primary" disabled={isSubmitting}
                                                className="shadow border border-light">Enviar</Button>
                                            { this.state.idServico 
                                                ? <Link className="btn btn-secondary shadow ml-4" to="/lstservico" >Voltar</Link>
                                                : null
                                            }
                                        </div>
                                    </fieldset>
                                </div>
                            </Form>
                        </Col>
                    </div>
                )}
            </Formik>
        );
    }}

export default CadServico;