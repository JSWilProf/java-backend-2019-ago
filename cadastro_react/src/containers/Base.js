import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { Jumbotron, Image, Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignInAlt, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux';
import { resetToken } from '../store/axios';

import Home from './Home';
import CadCliente from './CadCliente';
import ListaCliente from './ListaCliente';
import CadServico from './CadServico';
import ListaServico from './ListaServico';
import CadUsuario from './CadUsuario';
import ListaUsuario from './ListaUsuario';
import Login from './Login';
import * as actionTypes from '../store/actions';

import fasto from '../fasto.png';

class Base extends Component {
    handleLogin(selectecKey) {
        switch(selectecKey) {
            case 'login':
                this.props.onLogin();
                this.props.history.push('/login');
                break;
            default:
                resetToken();
                this.props.onLogout();
                this.props.history.push('/');
        }
    }

    render() {
        return (
            <Container>
                <Jumbotron className="text-center">
                    <Image src={fasto} fluid/>
                </Jumbotron>
                <div style={this.props.telaLogin ? {display:'none'} : null}>
                    <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark"
                        className="d-flex justify-content-between">
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto" >
                                <NavDropdown title="Clientes">
                                    { this.props.role !== ''
                                    ? <LinkContainer to="/cadcliente">
                                            <NavDropdown.Item>Cadastra Cliente</NavDropdown.Item>
                                        </LinkContainer>
                                    : null
                                    }  
                                    <LinkContainer to="/lstcliente">
                                        <NavDropdown.Item>Lista Clientes</NavDropdown.Item>
                                    </LinkContainer>
                                </NavDropdown>
                                <NavDropdown title="Serviços">
                                    { this.props.role !== ''
                                    ? <LinkContainer to="/cadservico">
                                            <NavDropdown.Item>Cadastra Serviço</NavDropdown.Item>
                                        </LinkContainer>
                                    : null
                                    }  
                                    <LinkContainer to="/lstservico">
                                        <NavDropdown.Item>Lista Serviços</NavDropdown.Item>
                                    </LinkContainer>
                                </NavDropdown>
                                { this.props.role === 'ROLE_ADMIN'
                                ? <NavDropdown title="Usuários">
                                        <LinkContainer to= "/cadusuario">
                                            <NavDropdown.Item>Cadastra Usuário</NavDropdown.Item>
                                        </LinkContainer>
                                        <LinkContainer to="/lstusuario">
                                            <NavDropdown.Item>Lista Usuários</NavDropdown.Item>
                                        </LinkContainer>
                                    </NavDropdown>
                                : null 
                                }
                            </Nav>
                            <Nav onSelect={selectedKey => this.handleLogin(selectedKey)}>
                                { this.props.userId === ''
                                ? <Nav.Link eventKey="login">
                                        login
                                        <span className="p-2">
                                            <FontAwesomeIcon icon={faSignInAlt} />
                                        </span>
                                    </Nav.Link>
                                : <Nav.Link eventKey="logout">
                                        logout {this.props.userId}
                                        <span className="p-2">
                                            <FontAwesomeIcon icon={faSignOutAlt} />
                                        </span>
                                    </Nav.Link>
                                }
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </div>

                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/cadcliente/:id" component={CadCliente} />
                    <Route path="/cadcliente" component={CadCliente} />
                    <Route path="/lstcliente" component={ListaCliente} />
                    <Route path="/cadservico" component={CadServico} />
                    <Route path="/lstservico" component={ListaServico} />
                    <Route path="/cadusuario/:id" component={CadUsuario} />
                    <Route path="/cadusuario" component={CadUsuario} />
                    <Route path="/lstusuario" component={ListaUsuario} />
                    <Route path="/login" component={Login} />
                </Switch>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.userId,
        role: state.role,
        telaLogin: state.telaLogin
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLogin: () => dispatch({type: actionTypes.LOGIN}),
        onLogout: () => dispatch({type: actionTypes.LOGOUT})
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Base));

  