import React, { Component } from 'react';
import { Col, Form, Button, Alert } from 'react-bootstrap';
import { Formik } from 'formik';
import * as yup from 'yup';
import axios from '../store/axios';
import { Link } from 'react-router-dom';

class CadCliente extends Component {
    state = {
        showAlert: false,
        tipoAlerta: '',
        mensagem: '',
        idCliente: this.props.match.params.id,
        cliente: {
            nome: '',
            endereco: '',
            telefone: '',
            email: ''
        }
    }

    componentDidMount() {
        if(this.state.idCliente) {
            axios.get('/editaCliente/' + this.state.idCliente
            ).then(response => {
                this.setState({
                    cliente: {
                        nome: response.data.nome,
                        endereco: response.data.endereco,
                        telefone: response.data.telefone,
                        email: response.data.email
                    }
                });
            }).catch(reason => {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "danger",
                    mensagem: reason.response.status === 403
                        ? 'Sem autorização para localizar o Cliente'
                        : 'Falha ao localizar o Cliente'
                });
            });    
        }
    }

    handleSubmit(values, {resetForm, setErrors, setSubmitting}) {
        axios.post('/cadastra', {
            idCliente: this.state.idCliente,
            nome: values.nome,
            endereco: values.endereco,
            telefone: values.telefone,
            email: values.email
        }).then(response => {
            if(this.state.idCliente) {
                this.props.history.push('/lstcliente');
            } else {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "success",
                    mensagem: 'Cadastrado com Sucesso',
                });
                resetForm();
                setSubmitting(false); 
            }
        }).catch(reason => {
            if(reason.response.status === 422) {
                setErrors(reason.response.data)
            } else {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "danger",
                    mensagem: reason.response.status === 403
                        ? 'Sem autorização para efetuar o Cadastro'
                        : 'Falha ao efetuar o Cadastro'
                });
            }
            setSubmitting(false); 
        });
    }

    handleDismiss = () => this.setState({ showAlert: false });

    render() {
        const schema = yup.object().shape({
            nome: yup.string()
                .min(5, 'O nome deve ter mais que 5 caracteres')
                .max(150, 'Não entre com mais de 150 caracteres')
                .required('O nome deve ser informado'),
            endereco: yup.string()
                .max(450, 'Não entre com mais de 450 caracteres')
                .required('O endereço deve ser informado'),
            telefone: yup.string()
                .matches(new RegExp('(9[0-9]{4}|[1-9][0-9]{3})-[0-9]{4}'),
                'O Número de telefone é inváido')
                .required('O Telefone deve ser informado'),
            email: yup.string()
                .email('O Email é inválido')
                .required('O E-Mail deve ser informado')
        });

        return (
            <Formik
                enableReinitialize
                validationSchema={schema}
                onSubmit={this.handleSubmit.bind(this)}
                initialValues={this.state.cliente}>
                {({handleSubmit, handleChange, values, touched, errors, isSubmitting}) => (
                    <div>
                        <Alert variant={this.state.tipoAlerta} onClose={this.handleDismiss} dismissible 
                            show={this.state.showAlert}>
                            <strong>{this.state.mensagem}</strong>
                        </Alert>
                        <Col className="mb-4 shadow p-4 quadro">
                            <Form noValidate onSubmit={handleSubmit}>
                                <div className="d-flex justify-content-center">
                                    <fieldset style={{flex: 1}}>
                                        <div>
                                        {this.state.idCliente 
                                             ? <h2>Alterar Cliente</h2>
                                             : <h2>Novo Cliente</h2>
                                            }
                                        </div>

                                        <Form.Group controlId="v01">
                                            <Form.Label>Nome</Form.Label>
                                            <Form.Control type="text" placeholder="Informe o Nome"
                                                name="nome" value={values.nome} onChange={handleChange}
                                                isInvalid={touched.nome && errors.nome} />
                                            <Form.Control.Feedback type="invalid">{errors.nome}</Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="v02">
                                            <Form.Label>Endereço</Form.Label>
                                            <Form.Control type="text" placeholder="Informe o Endereço"
                                                name="endereco" value={values.endereco} onChange={handleChange}
                                                isInvalid={touched.nome && errors.endereco} />
                                            <Form.Control.Feedback type="invalid">{errors.endereco}</Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="v03">
                                            <Form.Label>Telefone</Form.Label>
                                            <Form.Control type="text" placeholder="Informe o nº de Telefone"
                                                name="telefone" value={values.telefone} onChange={handleChange}
                                                isInvalid={touched.nome && errors.telefone} />
                                            <Form.Control.Feedback type="invalid">{errors.telefone}</Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="v04">
                                            <Form.Label>E-Mail</Form.Label>
                                            <Form.Control type="email" placeholder="Informe o E-Mail"
                                                name="email" value={values.email} onChange={handleChange}
                                                isInvalid={touched.email && errors.email} />
                                            <Form.Control.Feedback type="invalid">{errors.email}</Form.Control.Feedback>
                                        </Form.Group>
                                        <div>
                                            <Button type="submit" value="primary" disabled={isSubmitting}
                                                className="shadow border border-light">Enviar</Button>
                                            { this.state.idCliente 
                                                ? <Link className="btn btn-secondary shadow ml-4" to="/lstcliente" >Voltar</Link>
                                                : null
                                            }
                                        </div>
                                    </fieldset>
                                </div>
                            </Form>
                        </Col>
                    </div>
                )}
            </Formik>
        );
    }
}

export default CadCliente;