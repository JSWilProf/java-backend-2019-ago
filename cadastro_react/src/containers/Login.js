import React, { Component } from 'react';
import { Col, Form, Button, Alert } from 'react-bootstrap';
import { Formik } from 'formik';
import * as yup from 'yup';
import axios, { setupToken } from '../store/axios';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions';

class Login extends Component {
    state = {
        showAlert: false,
        tipoAlerta: '',
        mensagem: ''
    }

    handleSubmit(values, {setSubmitting}) {
        axios.post('/auth/signin', {
           username: values.nome,
           password: values.senha
        }).then(response => {
            setupToken(response.data.accessToken);
            this.getAutorizacao(values.nome);
       }).catch(reason => {
            this.setState({
               showAlert: true,
               tipoAlerta: "danger",
               mensagem: 'Falha ao efetuar o Login'
            });
            setSubmitting(false); 
       });
    }

    getAutorizacao(userId) {
        axios.get('/leAutorizacoes/' + userId
        ).then(response => {
            this.props.onLoginOk(userId, response.data.authority);
            this.props.history.push('/');
        }).catch(reason => {
            console.log(reason);
            this.setState({
               showAlert: true,
               tipoAlerta: "danger",
               mensagem: 'Falha ao efetuar o Login'
            });
        });
    }

    handleDismiss = () => this.setState({ showAlert: false });

    render() {
        const schema = yup.object().shape({
            nome: yup.string()
                .min(5, 'O nome deve ter mais que 4 caracteres')
                .max(15, 'Não entre com mais de 15 caracteres')
                .required('O nome do Usuáio deve ser informado'),
            senha: yup.string()
                .min(5, 'A senha deve ter mais que 4 caracteres')
                .max(20, 'Não entre com mais de 20 caracteres')
                .required('A Senha deve ser informado')
        });

        return (
            <Formik
                enableReinitialize
                validationSchema={schema}
                onSubmit={this.handleSubmit.bind(this)}>
                {({handleSubmit, handleChange, values, touched, errors, isSubmitting}) => (
                    <div>
                        <Alert variant={this.state.tipoAlerta} onClose={this.handleDismiss} dismissible 
                            show={this.state.showAlert}>
                            <strong>{this.state.mensagem}</strong>
                        </Alert>
                        <Col className="mb-4 shadow p-4 quadro">
                            <div className="d-flex justify-content-center ">
                                <Form noValidate onSubmit={handleSubmit}>
                                    <Form.Group controlId="v01">
                                        <Form.Label>Nome</Form.Label>
                                        <Form.Control type="text" placeholder="Usuário"
                                            name="nome" value={values.nome} onChange={handleChange}
                                            isInvalid={touched.nome && errors.nome}/>
                                        <Form.Control.Feedback type="invalid">{errors.nome}</Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group controlId="v02" >
                                        <Form.Label>Senha</Form.Label>
                                        <Form.Control type="password" placeholder="Senha"
                                            name="senha" value={values.senha} onChange={handleChange}
                                            isInvalid={touched.nome && errors.senha} />
                                        <Form.Control.Feedback type="invalid">{errors.senha}</Form.Control.Feedback>
                                    </Form.Group>
                                    <Button type="submit" value="primary" disabled={isSubmitting}
                                        className="shadow border border-light">Enviar</Button>
                                </Form>
                            </div>
                        </Col>
                    </div>
                )}
            </Formik>
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.userId,
        role: state.role,
        telaLogin: state.telaLogin
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLoginOk: (userId, role) => dispatch({type: actionTypes.LOGIN_OK, userId: userId, role: role}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);