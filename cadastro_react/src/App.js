import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Base from './containers/Base';
import { createStore } from 'redux';
import { Provider } from "react-redux";

import './App.css';
import reducer from './store/reducer';

const store = createStore(reducer);

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Base/>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
