import React, { Component } from 'react';
import { Modal, Button, Table, Form } from 'react-bootstrap';
import axios from '../store/axios';

class DialogoServicos extends Component {
    state = {
        show: false,
        idCliente: -1,
        servicos: []
    }

    handleSubmit() {
        axios.post('/selecionaServico', {
            idCliente: this.state.idCliente,
            servicos: this.state.servicos
        }).then(response => {
            this.props.handleMessage('success','Os Serviços foram atualizados');
        }).catch(reason => {
            this.props.handleMessage('danger','Houve falha na atualização dos Serviços');
        });

        this.setState({ 
            show: false,
            idCliente: -1,
            servicos: []
        });
    }

    handleClose = () => this.setState({ show: false });
 
    onChangeDelete(idServico) {
        const servico = this.state.servicos.find(svc => svc.idServico === idServico);
        var novoServico = Object.assign({}, servico);
        novoServico.selecionado = !servico.selecionado;

        const novosServicos = this.state.servicos.filter(svc => svc.idServico !== idServico)
        novosServicos.push(novoServico); 
        novosServicos.sort((a, b) => a.idServico - b.idServico);
        
        this.setState({servicos: novosServicos});
    }

    componentDidMount() {
        this.props.onRef(this)
    }
    
    componentWillUnmount() {
        this.props.onRef(undefined)
    }
    
    render() {
        const detalhes = this.state.servicos.map(servico => {
            return <Detalhe
                key={servico.idServico}
                idServico={servico.idServico}
                nome={servico.nome}
                selecionado={servico.selecionado}
                onChange={this.onChangeDelete.bind(this, servico.idServico)}/>
        })

        return (
            <Modal show={this.state.show} onHide={this.handleClose}>
                <Form noValidate>
                    <Modal.Header className="bg-secondary">
                        <Modal.Title>Inclusão de Serviços</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Table borderless>
                            <tbody className="mx-auto p-2 border border-secondary rounded">
                                {detalhes}
                            </tbody>
                        </Table>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" className="shadow border border-light" 
                            onClick={this.handleSubmit.bind(this)}>
                            Salvar
                        </Button>
                        <Button variant="success" className="shadow border border-light" 
                            onClick={this.handleClose}>
                            Fechar
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        );
    }
}

class Detalhe extends Component {
    state = {
        selecionado: this.props.selecionado
    }

    render() {
        return(
            <tr>
                <td className="w-100">{this.props.nome}</td>
                <td className="float-right pr-2">
                    <Form.Check type="checkbox" 
                        checked={this.state.selecionado}
                        value={this.props.idServico} 
                        onChange={(event) => {
                            const state = !this.state.selecionado;
                            this.setState({ selecionado: state });
                            this.props.onChange();
                        }}/> 
               </td>
            </tr>
        );
    }
}

export default DialogoServicos;