import * as actionTypes from './actions';

const initialState = {
    userId: '',
    role: '',
    telaLogin: false
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.LOGIN:
            return {
                ...state,
                telaLogin: true
            }
        case actionTypes.LOGIN_OK:
            return {
                ...state,
                telaLogin: false,
                userId: action.userId,
                role: action.role
            }
        case actionTypes.LOGOUT:
            return {
                ...state,
                userId: '',
                role: ''
            }
        default:
            return state;
    }
}

export default reducer;