package br.senai.sp.informatica.fundamentos.respostas;

import java.util.Comparator;
import java.util.stream.Stream;
import static java.util.stream.Collectors.*;

import javax.swing.JOptionPane;

public class Exercicio5Plus {
	public static void main(String[] args) {

		JOptionPane.showMessageDialog(null, "Nºs Ordenados\n\n" +
			Stream.generate(() -> Integer.parseInt(JOptionPane.showInputDialog("Informe o º nº")))
				.limit(5)
				.sorted(Comparator.reverseOrder())
				.map(num -> String.valueOf(num))
				.collect(joining(" "))
			);
	}

}
