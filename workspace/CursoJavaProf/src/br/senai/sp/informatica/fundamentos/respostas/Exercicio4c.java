package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio4c {
	public static void main(String[] args) {
		String[] listaDeMeses = {
			"Janeiro", "Fereiro", "Março", "Abril", 
			"Maio", "Junho", "Julho", "Agosto", "Setembro", 
			"Outubro", "Novembro", "Dezembro"
		};
		
		int mes = leMes();
		
		while(mes > 0) {
			String nomeDoMes = mes <= listaDeMeses.length ? listaDeMeses[mes - 1] : "Inválido";
			
			JOptionPane.showMessageDialog(null, "O nome do mês é: " + nomeDoMes);
			
			mes = leMes();
		}
	}
	
	public static int leMes() {
		int numeroDoMes;
		while(true) {
			try {
				numeroDoMes = Integer.parseInt(JOptionPane.showInputDialog("Informe o nº do mês"));
				break;
			} catch (NumberFormatException erro) {
				JOptionPane.showMessageDialog(null, "O Número do mês informado é inválido");
			}
		}
		return numeroDoMes;
	}
}
