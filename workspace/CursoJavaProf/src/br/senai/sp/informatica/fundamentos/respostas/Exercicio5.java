package br.senai.sp.informatica.fundamentos.respostas;

import java.util.Arrays;

import javax.swing.JOptionPane;

public class Exercicio5 {
	public static void main(String[] args) {
		int[] lista = new int[5];
		
		for (int i = 0; i < lista.length; i++) {
			lista[i] = Integer.parseInt(
					JOptionPane.showInputDialog("Informe o " + (i+1) + "º Nº"));
		}
		
		Arrays.sort(lista);
		
		String msg = "Nºs Ordenados\n\n";
		for (int i = lista.length - 1; i >= 0; i--) {
			msg += lista[i] + " ";
		}
		JOptionPane.showMessageDialog(null, msg);
	}
}
