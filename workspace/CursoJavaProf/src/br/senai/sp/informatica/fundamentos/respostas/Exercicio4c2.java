package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio4c2 {
	public static void main(String[] args) {
		String[] listaDeMeses = {
			"Janeiro", "Fereiro", "Março", "Abril", 
			"Maio", "Junho", "Julho", "Agosto", "Setembro", 
			"Outubro", "Novembro", "Dezembro"
		};
		
		String valor = leMes();
		
		while(!valor.equalsIgnoreCase("sair")) {
			int mes = Integer.parseInt(valor);
			String nomeDoMes = mes <= listaDeMeses.length ? listaDeMeses[mes - 1] : "Inválido";
			
			JOptionPane.showMessageDialog(null, "O nome do mês é: " + nomeDoMes);
			
			valor = leMes();
		}
	}
	
	public static String leMes() {
		while(true) {
			String valorInformado = JOptionPane.showInputDialog("Informe o nº do mês");
			
			if( valorInformado.equalsIgnoreCase("sair") || 
				valorInformado.compareTo("1") >= 0 && valorInformado.compareTo("12") <= 0)
				return valorInformado;
			else 	
				JOptionPane.showMessageDialog(null, "O nome do mês é: inválido 2");
		}
	}
}
