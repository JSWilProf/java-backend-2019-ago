package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio4b {
	public static void main(String[] args) {
		String[] listaDeMeses = {
			"Janeiro", "Fereiro", "Março", "Abril", 
			"Maio", "Junho", "Julho", "Agosto", "Setembro", 
			"Outubro", "Novembro", "Dezembro"
		};
		
		int mes = Integer.parseInt(JOptionPane.showInputDialog("Informe o nº do mês"));
		
		while(mes > 0) {
			String nomeDoMes = mes <= listaDeMeses.length ? listaDeMeses[mes - 1] : "Inválido";
			
			JOptionPane.showMessageDialog(null, "O nome do mês é: " + nomeDoMes);
			
			mes = Integer.parseInt(JOptionPane.showInputDialog("Informe o nº do mês"));
		}
	}
}
