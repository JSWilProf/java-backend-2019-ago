package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio4a {
	public static void main(String[] args) {
		int mes = Integer.parseInt(JOptionPane.showInputDialog("Informe o nº do mês"));
		
		while(mes > 0) {
			String nomeDoMes;
			
			if(mes == 1) nomeDoMes = "Janeiro";
			else if(mes == 2) nomeDoMes = "Fereiro";
			else if(mes == 3) nomeDoMes = "Março";
			else if(mes == 4) nomeDoMes = "Abril";
			else if(mes == 5) nomeDoMes = "Maio";
			else if(mes == 6) nomeDoMes = "Junho";
			else if(mes == 7) nomeDoMes = "Julho";
			else if(mes == 8) nomeDoMes = "Agosto";
			else if(mes == 9) nomeDoMes = "Setembro";
			else if(mes == 10) nomeDoMes = "OUtubro";
			else if(mes == 11) nomeDoMes = "Novembro";
			else if(mes == 12) nomeDoMes = "Dezembro";
			else nomeDoMes = "Inválido";

			JOptionPane.showMessageDialog(null, "O nome do mês é: " + nomeDoMes);
			
			mes = Integer.parseInt(JOptionPane.showInputDialog("Informe o nº do mês"));
		}
	}
}
