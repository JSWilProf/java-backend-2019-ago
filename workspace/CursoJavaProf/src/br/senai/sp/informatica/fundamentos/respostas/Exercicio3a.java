package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio3a {
	public static void main(String[] args) {
		String msg = "";
		int total = 1;
		for (int i = 1; i < 16; i++) {
			if(i % 2 != 0) {
				msg += i + " X ";
				total *= i;
			}
		}
		JOptionPane.showMessageDialog(null, msg + "\nTotal: " + total);
	}
}
