package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio3 {
	public static void main(String[] args) {
		String msg = "";
		int total = 1;
		for (int i = 1; i < 16; i+= 2) {
			msg += i + " X ";
			total *= i;
		}
		JOptionPane.showMessageDialog(null, msg + "\nTotal: " + total);
	}
}
