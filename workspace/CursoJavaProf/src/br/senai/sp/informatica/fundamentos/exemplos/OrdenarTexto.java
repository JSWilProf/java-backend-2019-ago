package br.senai.sp.informatica.fundamentos.exemplos;

import java.util.List;
import java.util.Arrays;
import java.util.Collections;

public class OrdenarTexto {
	public static void main(String[] args) {
		List<String> lista = Arrays.asList(
			"1", "2", "20", "12", "11", "111", "101"
		);
		
		Collections.sort(lista);
		
		for (String num : lista) {
			System.out.println(num);
		}
	}
}
