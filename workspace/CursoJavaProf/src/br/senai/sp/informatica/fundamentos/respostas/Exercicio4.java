package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio4 {
	public static void main(String[] args) {
		int mes = Integer.parseInt(JOptionPane.showInputDialog("Informe o nº do mês"));
		
		while(mes > 0) {
			String nomeDoMes;
			
			switch (mes) {
				case 1: nomeDoMes = "Janeiro"; break;
				case 2: nomeDoMes = "Fereiro"; break;
				case 3: nomeDoMes = "Março"; break;
				case 4: nomeDoMes = "Abril"; break;
				case 5: nomeDoMes = "Maio"; break;
				case 6: nomeDoMes = "Junho"; break;
				case 7: nomeDoMes = "Julho"; break;
				case 8: nomeDoMes = "Agosto"; break;
				case 9: nomeDoMes = "Setembro"; break;
				case 10: nomeDoMes = "OUtubro"; break;
				case 11: nomeDoMes = "Novembro"; break;
				case 12: nomeDoMes = "Dezembro"; break;
				default: nomeDoMes = "Inválido"; break;
			}
			JOptionPane.showMessageDialog(null, "O nome do mês é: " + nomeDoMes);
			
			mes = Integer.parseInt(JOptionPane.showInputDialog("Informe o nº do mês"));
		}
	}
}
